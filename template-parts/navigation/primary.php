<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<header class="primary-nav-container">
	<a href="#content" class="skip-nav button">Skip to Content</a>

	<div class="header-phone">
		<span>
			<p>Call for a quote</p>

			<?php if( get_field('lp_phone_number') ) { ?>
			<a href="tel:+1<?php the_field('lp_phone_number') ?>"><h2><?php the_field('lp_phone_number') ?></h2></a>
			<?php } else { ?>
			<a href="tel:+12482461669"><h2>248-246-1669</h2></a>
			<?php } ?>

		</span>
	</div>

	<div class="header-brand">
		<span>
			<a class="brand" href="<?= esc_url(home_url('/')); ?>">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2017/12/caps-remodeling-logo.png" alt="CAPS Remodeling Logo" />
			</a>
		</span>
	</div>

	<div class="header-usability">
		<span>
			<div id="controls" class="text-size">
				<p>Change text size</p>
				<ul class="textresizer">
					<li><a id="medium" class="regular button" href="#" title="Medium">A</a></li>
					<li><a id="large" class="large button" href="#" title="Large">A</a></li>
					<li><a id="extra-large" class="extra-large button" href="#" title="Extra Large">A</a></li>
				</ul>
			</div>
			<div class="read-page">
				<button class="speakit">Read this page to me</button>
			</div>
		</span>
	</div>

</header>
<div id="maincontent">