<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php get_header(); ?>

	<?php get_template_part('template-parts/posts/content', 'title'); ?>

	<?php get_template_part('template-parts/navigation/breadcrumb'); ?>

	<?php while (have_posts()) : the_post(); ?>
		<?php get_template_part( 'template-parts/posts/formats/format', get_post_format() ); ?>
	<?php endwhile; ?>

	<?php if ( comments_open() || get_comments_number() ) : ?>
		<?php comments_template(); ?>
	<?php endif; ?>

<?php get_footer(); ?>