<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php //LOGIC TO DETERMINE FEATURED IMAGE
$featured_image_url = 'https://via.placeholder.com/1920x1080';
?>

<article class="post-preview">
	<a href="<?php the_permalink(); ?>">
		<div class="featured-image" style="background-image: url('<?php echo $featured_image_url; ?>');"></div>
	</a>
	<a href="<?php the_permalink(); ?>">
		<h2><?php the_title(); ?></h2>
	</a>
	<?php if ( get_post_type() == 'post' ) { ?>
		<div class="meta">
			<?php get_template_part('template-parts/posts/entry-meta'); ?>
		</div>
	<?php } ?>
	<?php the_excerpt(); ?>
	<a href="<?php the_permalink(); ?>" class="button is-small">Learn More</a>
</article>