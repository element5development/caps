<?php 
/*-------------------------------------------------------------------
    Template Name: Landing/Marketing
-------------------------------------------------------------------*/
?>

<?php get_header($landing); ?>

<?php 

$hero = get_field('banner_image'); ?>

<section class="lp-hero" style="background-image: url(<?php echo $hero['url']; ?>);">
	<div class="hero-overlay">
		<h1><?php the_field('banner_headline'); ?></h1>
		<p><?php the_field('banner_subheader'); ?></p>
		<?php 

		$link = get_field('banner_button');

		if( $link ): ?>
			
			<a class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>

		<?php endif; ?>

	</div>
</section>

<a id="content" class="anchor"></a>

<section class="lp-content">
	<div class="content">
		<h2><?php the_field('left_headline'); ?></h2>
		<h3><?php the_field('left_subheader'); ?></h3>

		<?php

		$slideimages = get_field('left_image_gallery');

		if( $slideimages ): ?>
			<div class="slider-contain">
				<?php foreach( $slideimages as $slideimage ): ?>
					<img src="<?php echo $slideimage['url']; ?>" />
				<?php endforeach; ?>
			</div>
		<?php endif; ?>

		<div class="left-bottom-content">
			<?php the_field('left_content'); ?>
		</div>
	</div>
	<div class="content">
		<?php the_field('right_content'); ?>
	</div>
</section>

<?php if( have_rows('testimonial_slider') ): ?>

<section class="lp-testimonials" data-articulate-ignore>
	<h2 class="big"><?php the_field('testimonial_header'); ?></h2>

	<div class="testimonial-slider-contain">
		
				<?php while( have_rows('testimonial_slider') ): the_row(); ?>

				<div class="testimonial-slide">

						<?php
						$testimonial = get_sub_field( 'testimonial' ); 
						$author = get_sub_field( 'testimonial_name' ); 
						?>
						<p><?php echo $testimonial; ?></p>
						<p class="author"><strong><?php echo $author; ?></strong></p>

				</div>

				<?php endwhile; ?>

  </div>

</section>

<?php endif; ?>

<section class="lp-faq">
	<h2 class="big"><?php the_field('faq_header'); ?></h2>
	<div class="faq-content">
		<?php the_field('faq_content'); ?>
	</div>
</section>
<svg id="bigTriangleColor" xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="70" viewBox="0 0 100 102" preserveAspectRatio="none">
  <path d="M50 0 L0 100 L100 100 Z"></path>
</svg>
<section class="lp-about">
	<div class="about-left">
		<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2017/12/caps-remodeling-logo.png" alt="CAPS Remodeling Logo" />
	</div>
	<div class="about-right">
		<h2><?php the_field('about_header'); ?></h2>
		<h3><?php the_field('about_subheader'); ?></h3>

		<?php $smallimg = get_field( 'about_image' ); ?>

		<img src="<?php echo $smallimg['url']; ?>" alt="<?php echo $smallimg['alt']; ?>" />
		<?php the_field('about_content'); ?>

	</div>
</section>
<section class="lp-contact">

	<?php $ctaimage = get_field( 'cta_image' ); ?>

	<div class="cta-left" style="background-image: url('<?php echo $ctaimage['url']; ?>')">
	</div>
	<div class="cta-right">
		<h2 class="big"><?php the_field('cta_header'); ?></h2>
		<h3><?php the_field('cta_subheader'); ?></h3>
		<?php the_field('cta_content'); ?>

		<?php $cta = get_field('cta_button');

		if( $cta ): ?>
			
			<a class="button" href="<?php echo $cta['url']; ?>" target="<?php echo $cta['target']; ?>"><?php echo $cta['title']; ?></a>

		<?php endif; ?>

	</div>
</section>

<?php get_footer($landing); ?>