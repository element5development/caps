<?php /*

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel vestibulum erat. Aliquam iaculis lectus
sit amet lorem posuere, at feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus, purus nulla 
lobortis diam, eget posuere massa quam a diam. Duis dignissim velit neque, sed faucibus nulla luctus
vitae.  

*/ ?>

</div>
<footer>
	<div class="footer-phone">
		<p>Call for a quote</p>

		<?php if( get_field('lp_phone_number') ) { ?>
		<a href="tel:+1<?php the_field('lp_phone_number') ?>"><h2><?php the_field('lp_phone_number') ?></h2></a>
		<?php } else { ?>
		<a href="tel:+12482461669"><h2>248-246-1669</h2></a>
		<?php } ?>

	</div>
	<div class="footer-copywrite">
		<p>© <?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?>. All Rights Reserved. <a href="<?php echo get_home_url(); ?>/privacy-statement/">Privacy Statement</a></p>
	</div>
	<div id="element5-credit">
		<a target="_blank" href="https://element5digital.com">
			<img src="https://element5digital.com/wp-content/themes/e5-starting-point/dist/images/element5_credit.svg" />
		</a>
	</div>
</footer>