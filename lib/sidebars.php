<?php

/*-----------------------------------------
		SIDEBARS - www.wp-hasty.com
-----------------------------------------*/
function sidebar_creation() {
		$args = array(
			'name'          => __( 'Primary Sidebar', 'starting-point' ),
			'class'         => 'sidebar',
			'before_widget' => '<li id="%1$s" class="widget %2$s">',
			'after_widget'  => '</li>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>',
		);
		register_sidebar($args);
}
add_action( 'widgets_init', 'sidebar_creation' );