var $ = jQuery;

$(document).ready(function() {
  /*------------------------------------------------------------------
  	Text to Speech
	------------------------------------------------------------------*/
  $(".speakit").click(function() {
    var speaking = $().articulate('isSpeaking');
    var paused = $().articulate('isPaused');
    if (speaking) {
      if (paused) {
        $().articulate('resume');
      } else {
        $().articulate('pause');
      }
    } else {
      $("#maincontent").articulate('speak');
    };
  });
  /*------------------------------------------------------------------
		Form input adding & removing classes
	------------------------------------------------------------------*/
  $('input:not([type=checkbox]):not([type=radio])').focus(function() {
    $(this).addClass('is-activated');
  });
  $('textarea').focus(function() {
    $(this).addClass('is-activated');
  });
  $('select').focus(function() {
    $(this).addClass('is-activated');
  });
  /*------------------------------------------------------------------
  	Element5 Site Credit Appear
  ------------------------------------------------------------------*/
  $('#element5-credit img').viewportChecker({
    classToAdd: 'visible',
    offset: 10,
    repeat: true,
  });
  /*------------------------------------------------------------------
  	Click to Copy
  ------------------------------------------------------------------*/
  new Clipboard('.click-to-copy');
  $('.click-to-copy').on('click', function() {
    $(this).addClass('copied');
    $this = $(this);
    setTimeout(function() {
      $this.removeClass('copied');
    }, 2000);
  });
  /*------------------------------------------------------------------
  	Logic to display load more button
	------------------------------------------------------------------*/
  if ($('.next.page-numbers').length) {
    $('.load-more').css('display', 'block');
  }
  /*------------------------------------------------------------------
  	Infinite Scroll Init on Default Blog
  ------------------------------------------------------------------*/
  $('.feed').infiniteScroll({
    path: '.next.page-numbers',
    append: '.post-preview',
    button: '.load-more',
    scrollThreshold: false,
    checkLastPage: true,
    status: '.page-load-status',
  });
  /*------------------------------------------------------------------
  	Slick Slider
	------------------------------------------------------------------*/
  $('.slider-contain').slick({
    mobileFirst: true,
    prevArrow: '<a class="prev-slide-contain"></a>',
    nextArrow: '<a class="next-slide-contain"></a>',
    cssEase: 'linear',
    autoplay: true,
    autoplaySpeed: 2000,
  });
  /*------------------------------------------------------------------
  	Testimonial Slider
	------------------------------------------------------------------*/
  $('.testimonial-slider-contain').slick({
    mobileFirst: true,
    prevArrow: '<a class="prev-test-slide-contain"></a>',
    nextArrow: '<a class="next-test-slide-contain"></a>',
    slidesToShow: 1,
    slidesToScroll: 1,
    cssEase: 'linear',
    autoplay: true,
    autoplaySpeed: 10000,
    responsive: [{
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
      }
    }]
  });
  /*------------------------------------------------------------------
  	Text Resizer
	------------------------------------------------------------------*/
  $("#medium").click(function(event) {
    event.preventDefault();
    $("h1").animate({ "font-size": "60px" });
    $("h2").animate({ "font-size": "36px" });
    $("h3").animate({ "font-size": "24px" });
    $("p").animate({ "font-size": "18px", });
    $(".button").animate({ "font-size": "16px", });
    $("h2.big").animate({ "font-size": "48px" });
    $(".regular.button").animate({ "font-size": "13px", });
    $(".large.button").animate({ "font-size": "15px", });
    $(".extra-large.button").animate({ "font-size": "18px", });
    $(".hero-overlay p").animate({ "font-size": "28px", });
    $(".hero-overlay .button").animate({ "font-size": "28px", });
    $("a h2").animate({ "font-size": "40px", });
    $(".testimonial-slide p").animate({ "font-size": "22px", });
    $(".cta-right .button").animate({ "font-size": "28px", });
    $("ul.arrow").animate({ "font-size": "16px" });
  });
  $("#large").click(function(event) {
    event.preventDefault();
    $("h1").animate({ "font-size": "64px" });
    $("h2").animate({ "font-size": "40px" });
    $("h3").animate({ "font-size": "28px" });
    $("p").animate({ "font-size": "22px", });
    $(".button").animate({ "font-size": "20px", });
    $("h2.big").animate({ "font-size": "52px" });
    $(".regular.button").animate({ "font-size": "13px", });
    $(".large.button").animate({ "font-size": "15px", });
    $(".extra-large.button").animate({ "font-size": "18px", });
    $(".hero-overlay p").animate({ "font-size": "32px", });
    $(".hero-overlay .button").animate({ "font-size": "32px", });
    $("a h2").animate({ "font-size": "44px", });
    $(".testimonial-slide p").animate({ "font-size": "26px", });
    $(".cta-right .button").animate({ "font-size": "32px", });
    $("ul.arrow").animate({ "font-size": "20px" });
  });
  $("#extra-large").click(function(event) {
    event.preventDefault();
    $("h1").animate({ "font-size": "68px" });
    $("h2").animate({ "font-size": "44px" });
    $("h3").animate({ "font-size": "32px" });
    $("p").animate({ "font-size": "26px", });
    $(".button").animate({ "font-size": "24px", });
    $("h2.big").animate({ "font-size": "56px" });
    $(".regular.button").animate({ "font-size": "13px", });
    $(".large.button").animate({ "font-size": "15px", });
    $(".extra-large.button").animate({ "font-size": "18px", });
    $(".hero-overlay p").animate({ "font-size": "36px", });
    $(".hero-overlay .button").animate({ "font-size": "36px", });
    $("a h2").animate({ "font-size": "48px", });
    $(".testimonial-slide p").animate({ "font-size": "30px", });
    $(".cta-right .button").animate({ "font-size": "36px", });
    $("ul.arrow").animate({ "font-size": "24px" });
  });
  $("a").click(function() {
    $("a").removeClass("selected");
    $(this).addClass("selected");
  });
});