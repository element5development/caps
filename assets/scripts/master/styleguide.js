var $ = jQuery;

$(document).ready(function() {
  /*------------------------------------------------------------------
		COLORS HEX | RGB | CMYK
	------------------------------------------------------------------*/
  var hex = '';
  var rgb = '';
  var cmyk = '';
  $('.color').each(function() {
    var rgb = $(this).find('.color-swatch').css('backgroundColor');
    hexc(rgb);
    hexToCMYK(hex);
    rgb = rgb.slice(4, -1);
    $(this).find('.click-to-copy').attr('data-clipboard-text', hex);
    $(this).find('.hex span').text(hex);
    $(this).find('.rgb span').text(rgb);
    $(this).find('.cmyk span').text(cmyk);
  });

  function hexc(colorval) {
    var parts = colorval.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    delete(parts[0]);
    for (var i = 1; i <= 3; ++i) {
      parts[i] = parseInt(parts[i]).toString(16);
      if (parts[i].length == 1) parts[i] = '0' + parts[i];
    }
    hex = '#' + parts.join('');
  }

  function hexToCMYK(hex) {
    var computedC = 0;
    var computedM = 0;
    var computedY = 0;
    var computedK = 0;
    hex = (hex.charAt(0) == "#") ? hex.substring(1, 7) : hex;
    var r = parseInt(hex.substring(0, 2), 16);
    var g = parseInt(hex.substring(2, 4), 16);
    var b = parseInt(hex.substring(4, 6), 16);
    // BLACK
    if (r == 0 && g == 0 && b == 0) {
      computedK = 1;
      return [0, 0, 0, 1];
    }
    computedC = 1 - (r / 255);
    computedM = 1 - (g / 255);
    computedY = 1 - (b / 255);
    var minCMY = Math.min(computedC, Math.min(computedM, computedY));
    computedC = (computedC - minCMY) / (1 - minCMY);
    computedM = (computedM - minCMY) / (1 - minCMY);
    computedY = (computedY - minCMY) / (1 - minCMY);
    computedK = minCMY;
    computedC = Math.round(computedC * 100);
    computedM = Math.round(computedM * 100);
    computedY = Math.round(computedY * 100);
    computedK = Math.round(computedK * 100);

    cmyk = [computedC, computedM, computedY, computedK];
  }

  /*------------------------------------------------------------------
  	FONT FAMLIY
  ------------------------------------------------------------------*/
  $('.font').each(function() {
    var font = $(this).find('.example').css('font-family');
    font = font.replace('"', '');
    font = font.replace('"', '');
    family = font;
    weight = ' ';
    if (font.indexOf(' ') >= 0) {
      family = font.substr(0, font.indexOf(' '));
      weight = font.substr(font.indexOf(' '));
    }
    $(this).find('.family').text(family);
    $(this).find('.weight').text(weight);
  });

});