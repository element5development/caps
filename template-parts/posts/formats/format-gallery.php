<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<article id="post-<?php the_ID(); ?>" class="post-default post-container">

	<section class="default-contents">
		<div class="meta-card">
			<time class="entry-time" datetime="<?php get_the_time('Y-m-d'); ?>">
				<?php echo get_the_time(get_option('date_format')); ?>
			</time>
			<p class="author">
				<?php echo get_the_author_link( get_the_author_meta( 'ID' ) ); ?>
			</p>
			<p class="categories">
				Categories: <?php echo get_the_category_list(', ') ?>
			</p>
			<p class="tags">
				<?php the_tags(); ?>
			</p>
		</div>

    <?php the_content(); ?>
  </section>

</article>